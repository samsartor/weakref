//! The weakref crate provides a `WeakRef<T>` type which is `Copy` like `&T` but
//! `'static` like `Arc<T>`. The best of both worlds!
//!
//! ```rust
//!# fn spawn(_: impl std::future::Future + Sync + Send + 'static) {}
//!# struct State { }
//!# let cancel = true;
//! use weakref::{WeakRef, OwnRef, pin};
//! use std::sync::Mutex;
//!
//! let state = OwnRef::new(Mutex::new("pending"));
//!
//! spawn(async move {
//!     let guard = pin();
//!     let Some(state) = state.weak.load(&guard) else { return };
//!     *state.lock().unwrap() = "finished";
//! });
//!
//! if cancel {
//!     drop(state);    
//! }
//! ```
//!
//! The weakref crate is inspired by the
//! [Vale programming language's generational references](https://verdagon.dev/blog/generational-references),
//! although there are some internal differences.
//!
//! ## Unsized Values
//!
//! Currently it is not possible to create an OwnRef from a `Box<T>` when `T` is unsized. Theoretically this
//! could be supported, but would rely on the [unstable ptr_metadata feature](https://github.com/rust-lang/rust/issues/81513).

use crossbeam_queue::SegQueue;
use std::cell::UnsafeCell;
use std::marker::PhantomData;
use std::mem::replace;
use std::ops::Deref;
use std::ptr::{addr_of, null};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::{cmp, fmt};

/// Pins the current thread so that weak references can be safely loaded.
///
/// Re-export of [crossbeam_epoch::pin](https://docs.rs/crossbeam-epoch/latest/crossbeam_epoch/fn.pin.html):
pub use crossbeam_epoch::pin;

/// Prevents a weak reference from being freed while in active use.
///
/// Re-export of [crossbeam_epoch::Guard](https://docs.rs/crossbeam-epoch/latest/crossbeam_epoch/struct.Guard.html):
pub use crossbeam_epoch::Guard;

/// Fundementally the weakref crate works by leaking small allocations.
/// However, these can be reused almost endlessly. Implementations
/// of this trait are used to recycle previously used allocations.
pub trait Recycler: Send + Sync + Copy + 'static {
    fn pop(&self) -> Option<RawRef>;
    fn push(&self, raw: RawRef);
}

/// The global recycler used by default, based on crossbeam-queue.
#[derive(Copy, Debug, Clone)]
pub struct DefaultRecycler;

static DEFAULT_RECYCLER: SegQueue<RawRef> = SegQueue::new();

impl Recycler for DefaultRecycler {
    fn pop(&self) -> Option<RawRef> {
        DEFAULT_RECYCLER.pop()
    }

    fn push(&self, raw: RawRef) {
        DEFAULT_RECYCLER.push(raw);
    }
}

static NULL_INDIRECTION: Indirection = Indirection {
    data: UnsafeCell::new(null()),
    current_gen: AtomicUsize::new(usize::MAX),
};

struct Indirection {
    data: UnsafeCell<*const ()>,
    current_gen: AtomicUsize,
}

unsafe impl Send for Indirection {}
unsafe impl Sync for Indirection {}

/// Recyclable pointer to a small allocation.
pub struct RawRef {
    indirection: &'static Indirection,
}

impl RawRef {
    fn new(ptr: *mut ()) -> Self {
        let indirection = Box::leak(Box::new(Indirection {
            current_gen: AtomicUsize::new(0),
            data: UnsafeCell::new(ptr),
        }));
        RawRef { indirection }
    }
}

/// Reference which is `Copy + Send + 'static`.
pub struct WeakRef<T> {
    raw: &'static Indirection,
    expected_gen: usize,
    phantom: PhantomData<T>,
}

impl<T> WeakRef<T> {
    /// Access the reference if the owner has not been dropped.
    ///
    /// Specifically, this atomically compares checks the generation counter of reference.
    pub fn load<'g>(self, guard: &'g Guard) -> Option<&'g T> {
        // As long as the generation number matches, the indirection will contain the same data as
        // when self was created. The guard will prevent any new deletions screwing up the reference
        // we return, but a deletion may already be in the process of running when we get here. Load
        // the current generation with acquire ordering so we are sure any such deletion is started
        // _after_ informing us by incrementing the number.
        let current_gen = self.raw.current_gen.load(Ordering::Acquire);
        if current_gen == self.expected_gen {
            let ptr = unsafe { *self.raw.data.get() };
            let ptr = ptr.cast::<T>();
            let _ = guard;
            unsafe { ptr.as_ref() }
        } else {
            // No longer the same object
            None
        }
    }

    /// Produce a reference which is never accessable.
    pub fn null() -> Self {
        WeakRef {
            raw: &NULL_INDIRECTION,
            expected_gen: 0,
            phantom: PhantomData,
        }
    }

    /// Returns true if this reference is inaccessable. Note that returning false is no guarentee of
    /// accessability, since the owner could be dropped at any time.
    pub fn is_null(self) -> bool {
        let current_gen = self.raw.current_gen.load(Ordering::Relaxed);
        current_gen != self.expected_gen
    }

    /// Returns true if this reference was returned by [WeakRef::null] specifically.
    pub fn is_exactly_null(self) -> bool {
        addr_of!(*self.raw) == addr_of!(NULL_INDIRECTION)
    }

    /// Some integers which uniquely identify this reference in space and time.
    pub fn identity(self) -> (usize, usize) {
        (addr_of!(*self.raw) as usize, self.expected_gen)
    }
}

impl<T> Clone for WeakRef<T> {
    fn clone(&self) -> Self {
        Self {
            raw: self.raw,
            expected_gen: self.expected_gen,
            phantom: PhantomData,
        }
    }
}

/// This impl is the whole point!
impl<T> Copy for WeakRef<T> {}

impl<T> cmp::PartialEq for WeakRef<T> {
    fn eq(&self, other: &Self) -> bool {
        self.identity() == other.identity()
    }
}

impl<T> cmp::Eq for WeakRef<T> {}

impl<T> fmt::Debug for WeakRef<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let guard = pin();
        match self.load(&guard) {
            Some(inner) => f.debug_tuple("WeakRef").field(inner).finish(),
            None => f.write_str("WeakRef::Null"),
        }
    }
}

/// The unique owner of a reference.
pub struct OwnRef<T: Send + 'static, R: Recycler = DefaultRecycler> {
    /// The weak version of this reference.
    pub weak: WeakRef<T>,
    recycler: R,
}

impl<T: Send + 'static> OwnRef<T> {
    /// Create a new reference which owns the given value. Equivalent to `OwnRef::from_box(Box::new(value))`.
    pub fn new(value: T) -> Self {
        OwnRef::new_with(value, DefaultRecycler)
    }

    /// Wrap the given box in an extra layer of indirection, to detect when it is dropped.
    ///
    /// Although weakref does operate by leaking memory under the hood, this does not apply
    /// to the given box, which will be freed almost as soon as the returned object is dropped.
    pub fn from_box(value: Box<T>) -> Self {
        OwnRef::from_box_with(value, DefaultRecycler)
    }
}

impl<T: Send + 'static, R: Recycler> OwnRef<T, R> {
    /// Same as [Self::new] but with a custom recycler.
    pub fn new_with(value: T, recycler: R) -> Self {
        Self::from_box_with(Box::new(value), recycler)
    }

    /// Same as [Self::from_box] but with a custom recycler.
    pub fn from_box_with(data: Box<T>, recycler: R) -> Self {
        let ptr = Box::into_raw(data).cast();
        let (raw, gen) = match recycler.pop() {
            Some(recycle) => {
                // SAFETY: we have exclusive access to any recycled indirection
                unsafe { *recycle.indirection.data.get() = ptr };
                // Technically we don't even need an atomic load, since we have exclusive access.
                let gen = recycle.indirection.current_gen.load(Ordering::Relaxed);
                (recycle, gen)
            }
            None => (RawRef::new(ptr), 0),
        };

        OwnRef {
            weak: WeakRef {
                raw: raw.indirection,
                expected_gen: gen,
                phantom: PhantomData,
            },
            recycler,
        }
    }

    /// Schedule this pointer to be destroyed and the indirection to be recyled, as soon as guard
    /// allows.
    ///
    /// SAFETY: `self` must not be used again afterwards
    unsafe fn delete(&mut self, guard: &Guard) {
        let recycler = self.recycler;
        let raw = RawRef {
            // RawRef is not Copy so that recycler can be safely implemented. But make a copy
            // here so that we do not move out of self.
            indirection: self.weak.raw,
        };
        // Increment the generation counter with Release ordering so that no [WeakGrc::load] can
        // access the indirection from this moment onward. If a load has already occured and the
        // pointer is running around somewhere, the cleanup will be defered until that thread is
        // unpinned. Otherwise it may occur immediately.
        let previous_gen = raw.indirection.current_gen.fetch_add(1, Ordering::Release);
        let call_delete = move || {
            // SAFETY: We now have exclusive access.
            let ptr_ref = unsafe { &mut *raw.indirection.data.get() };
            let ptr = replace(ptr_ref, null());
            // SAFETY: The type can not have changed without calling `delete` previously.
            let ptr = ptr.cast::<T>().cast_mut();
            // SAFETY: The pointer was created by Box
            unsafe { Box::from_raw(ptr) };
        };
        if previous_gen == usize::MAX - 1 {
            guard.defer(move || {
                // Delete the data.
                call_delete();
                // We can not invalidate the indirection if we use it again. Free the memory
                // but leak the indirection forever! Ideally this never happens but better
                // safe than triggering crazy UB.
            });
        } else {
            guard.defer(move || {
                // Delete the data.
                call_delete();
                // And recycle the indirection!
                recycler.push(raw);
            });
        }
    }

    /// Access this reference similarly to [Self::deref], but borrowing the given guard instead of self.
    pub fn load<'g>(this: &Self, guard: &'g Guard) -> &'g T {
        // It is only possible to invalidate or concurrently modify the data pointer by dropping
        // or detaching self. Even when self is dropped, the guard has pinned the current thread
        // so any such modifications are deferred until the guard is also dropped.
        let ptr = unsafe { *this.weak.raw.data.get() };
        let ptr = ptr.cast::<T>();
        let reference = unsafe { ptr.as_ref() };
        let _ = guard;
        reference.unwrap()
    }
}

impl<T: Send + 'static> Deref for OwnRef<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        // It is only possible to invalidate or concurrently modify the data pointer by dropping self.
        let ptr = unsafe { *self.weak.raw.data.get() };
        let ptr = ptr.cast::<T>();
        unsafe { ptr.as_ref() }.unwrap()
    }
}

impl<T: Send + 'static> fmt::Debug for OwnRef<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("OwnRef")
            .field(Self::load(self, &pin()))
            .finish()
    }
}

impl<T: Send + 'static> From<Box<T>> for OwnRef<T> {
    fn from(value: Box<T>) -> Self {
        Self::from_box(value)
    }
}

impl<T: Send + 'static, R: Recycler> Drop for OwnRef<T, R> {
    fn drop(&mut self) {
        // TODO: if there are no other pins, we don't really need to pin this either
        let guard = pin();
        // SAFETY: we are inside Drop, so this only runs once
        unsafe { self.delete(&guard) };
    }
}
